/*global $,owl,prettyPhoto*/
$(document).ready(function () {
    "use strict";

    // For Trigger All Products Slider
    $(".products-slider").owlCarousel({
        navigation: true,
        slideSpeed: 500,
        responsive: true,
        autoPlay: true,
        navigationText: ["<img src='images/left.png'>", "<img src='images/right.png'>"],
        pagination: false,
        items: 4,
        stopOnHover: true
    });

    // For Trigger All Comparison Slider
    $(".compare-slider").owlCarousel({
        navigation: false,
        slideSpeed: 500,
        responsive: true,
        autoPlay: true,
        navigationText: ["<img src='images/left.png'>", "<img src='images/right.png'>"],
        pagination: false,
        items: 10,
        stopOnHover: true
    });

    // For Trigger All Percenteges Slider
    $(".graph-slider").owlCarousel({
        navigation: false,
        slideSpeed: 500,
        responsive: true,
        autoPlay: false,
        navigationText: ["<img src='images/left.png'>", "<img src='images/right.png'>"],
        pagination: false,
        items: 10,
        stopOnHover: true
    });


    $('.filters a').click(function () {
        $(this).addClass('active').siblings().removeClass('active');
    });

    $('.basic').fancySelect();

    $('.show-ajax').click(function () {
        $('.ajax-hidden').show();
    });

    $('.show-compare1').click(function () {
        $('.compare1').show();
        $('.compare2').hide();
    });

    $('.show-compare2').click(function () {
        $('.compare2').show();
        $('.compare1').hide();
    });

    // for prettyphoto images gallery
    $("a[rel^='prettyPhoto']").prettyPhoto({
        theme: 'light_square',
        social_tools: false,
        deeplinking: false
    });

});