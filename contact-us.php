<?php include 'header.php' ?>

    <div class="contact-head col-xs-12 text-center">
        <div class="container">
            <h1>للتواصل معنا </h1>
        </div>
        <!-- end container -->
    </div>
    <!-- end contact-head -->
    <div class="contact-wrapper col-xs-12">
        <div class="container">
            <div class="send-msg col-md-6 col-xs-12 navbar-right">
                <form>
                    <input type="email" required placeholder="إكتب بريدك الالكتروني من هنا">
                    <textarea placeholder="رسالتك للموقع"></textarea>
                    <button type="submit">إرسال</button>
                </form>
            </div>
            <div class="contacts col-md-6 col-xs-12 text-right">
                <div>
                    <h1>
                        أرقام الموبايل
                        <i class="fa fa-mobile"></i>
                    </h1>

                    <p>0123456789</p>
                </div>
                <div>
                    <h1>
                        الايميلات
                        <i class="fa fa-envelope-o"></i>
                    </h1>

                    <p>abc@store.com</p>
                </div>
                <div>
                    <h1>
                        العنوان
                        <i class="fa fa-car"></i>
                    </h1>

                    <p>22 old st, maadi</p>
                </div>
                <div>
                    <h1>
                        معلومات التواصل
                        <i class="fa fa-share-alt"></i>
                    </h1>

                    <div>
                        <a href="#" class="facebook">
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a href="#" class="twitter">
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a href="#" class="linkedin">
                            <i class="fa fa-linkedin"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- end container -->
    </div>
    <!-- end contact-wrapper -->
<?php include 'footer.php' ?>