<?php include 'header.php';

$categories = listCategories();
$types = listTypes();
$products = lastTenProducts();
if($_POST['type_id']){
    header('Location: comparison.php?id='.$_POST['type_id']);
    die();
}
if($_POST['category_id']){
    header('Location: all-types.php?id='.$_POST['category_id']);
    die();
}
?>
    <div class="search-box col-xs-12">
        <div class="container">
            <div class="search-form col-xs-12 text-center">
<!--                <form action="#" method="get">-->
<!--                    <button type="submit">-->
<!--                        <i class="fa fa-search"></i>-->
<!--                    </button>-->
<!--                    <input type="search" placeholder="إبحث عن القسم والصنف من هنا">-->
<!--                </form>-->
            </div>
            <!-- end search-form -->
        </div>
        <!-- end container -->
    </div>
    <!-- end search-box -->

    <div class="selects-box col-xs-12 text-center">
        <form action="<?=$_SERVER['PHP_SELF']?>" method="POST">
        <div class="container">
            <div class="col-md-5 col-xs-12 navbar-right">
                <select  id="category" name="category_id">
                    <option value="">-- إختر القسم --</option>
                    <?php if(!empty($categories)): ?>
                        <?php foreach ($categories as $category): ?>
                            <option value="<?= $category['id'] ?>"><?= $category['name'] ?></option>
                        <?php endforeach ?>
                    <?php endif ?>
                </select>
            </div>
            <div class="col-md-5 col-xs-12 navbar-right">
                <select  id="types" name="type_id">
                    <option>-- إختر الصنف --</option>
                </select>
            </div>
            <div class="col-md-2 col-xs-12">
                <button type="submit">
                    <i class="fa fa-flash"></i> ذهاب
                </button>
            </div>
            </form>
        </div>
        <!-- end container -->
    </div>
    <!-- end selects-box -->

    <div class="most-products col-xs-12 text-center">
        <div class="products-head col-xs-12">
            <h1>أحدث المنتجات المضافه</h1>
        </div>
        <!-- end products-head -->
        <div class="container">
            <div class="products-slider col-xs-12">
                <?php foreach ($products as $product): ?>
                    <div class="item">
                        <div class="item-content">
                            <div class="item-inner">
                                <div class="item-caption col-xs-12">
                                    <a href="comparison.php">
                                        <i class="fa fa-link"></i>
                                    </a>
                                </div>
                                <!-- end item-caption -->
                                <div class="item-img">
                                    <img src="<?= $product['pic'] ?>" alt="prod1">
                                </div>
                                <!-- end item-img -->
                                <div class="item-name">
                                    <p>
                                        <?= $product['name'] ?>
                                    </p>
                                </div>
                                <!-- end item-name -->
                            </div>
                            <!-- end item-inner -->
                        </div>
                        <!-- end item-content -->
                    </div>
                    <!-- end item -->
                <?php endforeach ?>
            </div>
            <!-- end products-slider -->
        </div>
        <!-- end container -->
    </div>
    <!-- end most-products -->

    <!--
    <div class="status col-xs-12">
    <div class="col-md-4">
        <img src="images/b3.jpg" alt="" class="img-responsive">
    </div>
    <div class="col-md-8">
        <p>
            كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني.
        </p>
    </div>
    </div>
    end status
    -->
<?php include 'footer.php'; ?>
<script type="text/javascript">
    $(document).ready(function (e) {
         $('#category').click(function (e) {
             var optionSelected = $(this);
             var id = optionSelected.val();
             console.log(id);
             e.preventDefault();
            $.ajax({
                type: 'POST',
                url : './ajax/getTypes.php',
                data: { 'category_id' : id},
                success : function (data) {
                    console.log(data);
                    $('#types').get(0).innerHTML = '<option>-- إختر الصنف --</option>';
                    data.forEach(function (entry) {
                        $('#types').get(0).innerHTML += "<option value="+entry.id+">"+entry.name+"</option>";
                    })
                },
                dataType: 'json'
            })
         });
    });
</script>