<?php include 'header.php';
if ($_GET['id']) {
    $id = $_GET['id'];
    $_SESSION['category_id'] = $id;
} else {
    $id = $_SESSION['category_id'];
}
if ($_POST['name']) {
    if (!empty($_FILES["myFile"])) {
        define("UPLOAD_DIR", $_SERVER['DOCUMENT_ROOT'] . '/top_10/uploads/');
        $myFile = $_FILES["myFile"];

        if ($myFile["error"] !== UPLOAD_ERR_OK) {
            echo "<p>An error occurred.</p>";
            exit;
        }

        // ensure a safe filename
        $name = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["name"]);

        // don't overwrite an existing file
        $i = 0;
        $parts = pathinfo($name);
        while (file_exists(UPLOAD_DIR . $name)) {
            $i++;
            $name = $parts["filename"] . "-" . $i . "." . $parts["extension"];
        }

        // preserve file from temporary directory
        $success = move_uploaded_file($myFile["tmp_name"], UPLOAD_DIR . $name);
        if (!$success) {
            echo "<p>Unable to save file.</p>";
            exit;
        }

        $pic = '/top_10/uploads/' . $name;
    }
    $name = $_POST['name'];
    addType($name, $pic, $id);
}
if ($_POST['delete']) {
    deleteType($_POST['delete']);
}
$types = getCategoryTypes($id);
?>
    <div id="page-wrapper">

        <div class="container-fluid">

            <div class="cont-serch text-right col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h1>إضافة صنف جديد</h1>
                <a class="add-sector">
                    <i class="fa fa-plus"></i>
                </a>
            </div>

            <div class="com-tabel col-lg-12 col-md-12 col-sm-12 col-xs-12 row">
                <?php if (isset($types)) : ?>
                    <table>
                        <tr>
                            <div class="col-lg-2">
                                <th>NO</th>
                            </div>
                            <div class="col-lg-8">

                                <th>Type Name</th>
                            </div>
                            <div class="col-lg-2">
                                <th>Controls</th>
                            </div>
                        </tr>
                        <?php foreach ($types as $type): ?>
                            <tr>
                                <div class="col-lg-2">
                                    <td><?= $type['id'] ?></td>
                                </div>
                                <div class="col-lg-8">
                                    <td><a href="all-types.php?id=<?= $type['id'] ?>"><?= $type['name'] ?></a></td>
                                </div>
                                <td>
                                    <!--                        data-toggle="tooltip" data-placement="top" title="Compare Elements"-->
                                    <div class="col-lg-1 ">
                                        <a class="btn btn-primary" href="/top_10/comparison.php?id=<?=$type['id']?>" style="display: inline-block" width="50%"><i
                                                class="fa fa-database"></i></a>
                                    </div>
                                    <div class="col-lg-1 ">

                                        <form action="<?= $_SERVER['PHP_SELF'] ?>" METHOD="post">
                                            <input type="hidden" name="delete" value="<?= $type['id'] ?>"/>
                                            <button type="submit" class="btn btn-danger" style="display: inline-block">
                                                <i
                                                    class="fa fa-close"></i></button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </table>
                <?php endif ?>
            </div>

            <div class="edit-sector col-md-4 col-sm-6 col-xs-12">
                <form action="<?= $_SERVER['PHP_SELF'] ?>" ENCTYPE="multipart/form-data" method="POST">
                    <div class="sector-img col-xs-12 text-center">
                        <img src="#" alt="Your image" class="target" data-toggle="tooltip" data-placement="top"
                             title="Image Show">

                        <div class="fileUpload col-xs-12 text-center">
                            <span><i class="fa fa-camera"></i> &nbsp; Upload Your type image</span>
                            <input type="file" name="myFile" class="upload" id="file">
                        </div>
                    </div>
                    <!-- end sector-img -->
                    <input type="text" name="name" placeholder="Edit type name ..">
                    <input type="submit" value="Save" class="save">
                </form>
            </div>

            <div class="edit-sector1 col-md-4 col-sm-6 col-xs-12">
                <form action="<?= $_SERVER['PHP_SELF'] ?>" ENCTYPE="multipart/form-data" method="POST">
                    <div class="sector-img col-xs-12 text-center">
                        <img src="#" alt="Your image" class="target" data-toggle="tooltip" data-placement="top"
                             title="Image Show">

                        <div class="fileUpload col-xs-12 text-center">
                            <span><i class="fa fa-camera"></i> &nbsp; Upload Your type image</span>
                            <input type="file" name="myFile" class="upload" id="file">
                        </div>
                    </div>
                    <!-- end sector-img -->
                    <input type="text" name="name" placeholder="Edit type name ..">
                    <input type="submit" value="Save" class="save">
                </form>
            </div>

            <div class="edit-sector2 col-md-4 col-sm-6 col-xs-12">
                    <span class="add-elements" data-toggle="tooltip" data-placement="bottom" title="Add new element">
                        <i class="fa fa-plus"></i>
                    </span>

                <div class="elements col-xs-12">
                    <input type="text" placeholder="type comparison element name ..">
                </div>
                <input type="submit" value="Save">
            </div>

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

<?php include "footer.php" ?>