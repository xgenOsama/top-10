<?php include 'header.php';
if ($_GET['id']) {
    $id = $_GET['id'];
    $_SESSION['type_id'] = $id;
} else {
    $id = $_SESSION['type_id'];
}
if ($_POST['name']) {
    if (!empty($_FILES["myFile"])) {
        define("UPLOAD_DIR", $_SERVER['DOCUMENT_ROOT'] . '/top_10/uploads/');
        $myFile = $_FILES["myFile"];

        if ($myFile["error"] !== UPLOAD_ERR_OK) {
            echo "<p>An error occurred.</p>";
            exit;
        }

        // ensure a safe filename
        $name = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["name"]);

        // don't overwrite an existing file
        $i = 0;
        $parts = pathinfo($name);
        while (file_exists(UPLOAD_DIR . $name)) {
            $i++;
            $name = $parts["filename"] . "-" . $i . "." . $parts["extension"];
        }

        // preserve file from temporary directory
        $success = move_uploaded_file($myFile["tmp_name"], UPLOAD_DIR . $name);
        if (!$success) {
            echo "<p>Unable to save file.</p>";
            exit;
        }

        $pic = '/top_10/uploads/' . $name;
    }
    $name = $_POST['name'];
    addProduct($name, $pic, $id);
}
if (isset($_POST['delete'])) {
    $product_id = $_POST['delete'];
    deleteProduct($product_id);
}
$products = getTypeProducts($id);
?>

    <div id="page-wrapper">

        <div class="container-fluid">

            <div class="cont-serch text-right col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h1>إضافة منتج جديد</h1>
                <a class="add-sector">
                    <i class="fa fa-plus"></i>
                </a>
            </div>

            <div class="com-tabel col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <table>
                    <tr>
                        <th>NO</th>
                        <th>Type Name</th>
                        <th>Controls</th>
                    </tr>
                    <?php foreach ($products as $product): ?>
                        <tr>
                            <th><?= $product['id'] ?></th>
                            <th><a href="type-elements.php?id=<?=$product['id']?>"><?= $product['name'] ?></a></th>
                            <th>
                                <form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
                                    <input type="hidden" name="delete" value="<?= $product['id'] ?>"/>
                                    <button type="submit" class="btn btn-danger"><i
                                            class="fa fa-close"></i></button>
                                </form>
                            </th>
                        </tr>
                    <?php endforeach ?>
                </table>
            </div>

            <div class="edit-sector1 col-md-4 col-sm-6 col-xs-12">
                <form action="<?= $_SERVER['PHP_SELF'] ?>" ENCTYPE="multipart/form-data" method="POST">
                    <div class="sector-img col-xs-12 text-center">
                        <img src="#" alt="Your image" class="target" data-toggle="tooltip" data-placement="top"
                             title="Image Show">

                        <div class="fileUpload col-xs-12 text-center">
                            <span><i class="fa fa-camera"></i> &nbsp; Upload Your type image</span>
                            <input type="file" name="myFile" class="upload" id="file">
                        </div>
                    </div>
                    <!-- end sector-img -->
                    <input type="text" name="name" placeholder="Edit type name ..">
                    <input type="submit" value="Save" class="save">
                </form>
            </div>
        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

<?php include 'footer.php' ?>