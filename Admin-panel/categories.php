<?php
include 'header.php';
if (isset($_POST['name'])) {
    if (!empty($_FILES["myFile"])) {
        define("UPLOAD_DIR", $_SERVER['DOCUMENT_ROOT'] . '/top_10/uploads/');
        $myFile = $_FILES["myFile"];

        if ($myFile["error"] !== UPLOAD_ERR_OK) {
            echo "<p>An error occurred.</p>";
            exit;
        }

        // ensure a safe filename
        $name = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["name"]);

        // don't overwrite an existing file
        $i = 0;
        $parts = pathinfo($name);
        while (file_exists(UPLOAD_DIR . $name)) {
            $i++;
            $name = $parts["filename"] . "-" . $i . "." . $parts["extension"];
        }

        // preserve file from temporary directory
        $success = move_uploaded_file($myFile["tmp_name"], UPLOAD_DIR . $name);
        if (!$success) {
            echo "<p>Unable to save file.</p>";
            exit;
        }

        $pic = '/top_10/uploads/' . $name;
    }

    $name = $_POST['name'];
    addCategory($name, $pic);
}
if (isset($_POST['delete'])) {
    $id = $_POST['delete'];
    deleteCategory($id);
}
$categories = listCategories();

?>
    <div id="page-wrapper">

        <div class="container-fluid">

            <div class="cont-serch text-right col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h1>إضافة قسم جديد</h1>
                <a class="add-sector">
                    <i class="fa fa-plus"></i>
                </a>
            </div>

            <div class="com-tabel col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <table>
                    <tr>
                        <th>NO</th>
                        <th>Category Name</th>
                        <th>Controls</th>
                    </tr>
                    <?php foreach ($categories as $category): ?>
                        <tr>
                            <th><?= $category['id'] ?></th>
                            <th><a href="types.php?id=<?= $category['id'] ?>"><?= $category['name'] ?></a></th>
                            <th>
                                <!--                        <span class="edit" data-toggle="tooltip" data-placement="top" title="Edit Sectore"><i-->
                                <!--                                class="fa fa-pencil"></i></span>-->

                                <form method="POST" action="<?= $_SERVER['PHP_SELF'] ?>">
                                    <input type="hidden" name="delete" value="<?= $category['id'] ?>"/>
                                    <button type="submit" style="background: #e80929" class="btn btn-danger"><i
                                            class="fa fa-close"></i></button>
                                </form>
                            </th>
                        </tr>
                    <?php endforeach ?>

                </table>
            </div>

            <div class="edit-sector col-md-4 col-sm-6 col-xs-12">
                <form method="POST" action="<?= $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data">
                    <div class="sector-img col-xs-12 text-center">
                        <img src="#" alt="Your image" class="target" data-toggle="tooltip" data-placement="top"
                             title="Image Show">

                        <div class="fileUpload col-xs-12 text-center">
                            <span><i class="fa fa-camera"></i> &nbsp; Upload Your sector image</span>
                            <input type="file" name="myFile" class="upload" id="file">
                        </div>
                    </div>
                    <!-- end sector-img -->
                    <input type="text" name="name" placeholder="Edit sector name ..">
                    <input type="submit" value="Save" class="save">
                </form>
            </div>

            <div class="edit-sector1 col-md-4 col-sm-6 col-xs-12">
                <form method="POST" action="<?= $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data">
                    <div class="sector-img col-xs-12 text-center">
                        <img src="#" alt="Your image" class="target" data-toggle="tooltip" data-placement="top"
                             title="Image Show">

                        <div class="fileUpload col-xs-12 text-center">
                            <span><i class="fa fa-camera"></i> &nbsp; Upload Your Product image</span>
                            <input type="file" name="myFile" class="upload" id="file1">
                        </div>
                    </div>
                    <!-- end sector-img -->
                    <input type="text" name="name" placeholder="Add product name ..">
                    <input type="submit" value="Add">
                </form>
            </div>

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

<?php include 'footer.php' ?>