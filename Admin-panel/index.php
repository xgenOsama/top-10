<?php include 'header.php';

?>

<div id="page-wrapper">

    <div class="container-fluid">
        <div class="block col-md-6 col-xs-12 text-center">
            <div class="block-cont blue">
                <h1>
                    <i class="fa fa-bank"></i>&nbsp;&nbsp;عدد الاقسام
                </h1>

                <h1><?=countCategories()?></h1>
            </div>
            <!-- end block-cont -->
        </div>
        <!-- end block -->

        <div class="block col-md-6 col-xs-12 text-center">
            <div class="block-cont green">
                <h1>
                    <i class="fa fa-cubes"></i>&nbsp;&nbsp;عدد الاصناف
                </h1>

                <h1><?=countTypes()?></h1>
            </div>
            <!-- end block-cont -->
        </div>
        <!-- end block -->

        <div class="search-box col-xs-12 text-center">
<!--            <form>-->
<!--                <input type="search" placeholder="search about you want ..">-->
<!--                <button type="submit">-->
<!--                    <i class="fa fa-search"></i>-->
<!--                </button>-->
<!--            </form>-->
        </div>
        <!-- end search-box -->
    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
<?php include 'footer.php' ?>

