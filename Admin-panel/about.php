<?php include 'header.php' ?>
    <div id="page-wrapper">

        <div class="container-fluid">
            <div class="element-wrapper about-elem col-md-6 col-xs-12">
                <div class="add-video col-xs-12">
                    <div class="fileUpload col-xs-12 text-center">
                        <span><i class="fa fa-video-camera"></i> &nbsp; Upload Your Video</span>
                        <input type="file" class="upload">
                    </div>
                </div>
                <!-- end add-video -->
                <div class="add-text col-xs-12">
                    <textarea placeholder="اضف هنا محتوي رؤية الموقع"></textarea>
                </div>
                <!-- end add-text -->

                <div class="add-text col-xs-12">
                    <textarea placeholder="اضف هنا محتوي رسالة الموقع"></textarea>
                </div>
                <!-- end add-text -->

                <div class="add-text col-xs-12">
                    <input type="text" placeholder="اضف رقم الموبايل">
                </div>
                <!-- end add-text -->

                <div class="add-text col-xs-12">
                    <input type="email" placeholder="اضف الايميل ">
                </div>
                <!-- end add-text -->

                <div class="add-text col-xs-12">
                    <input type="text" placeholder="اضف العنوان">
                </div>
                <!-- end add-text -->

                <div class="add-text col-xs-12">
                    <input type="text" placeholder="facebook">
                    <input type="text" placeholder="twitter">
                    <input type="text" placeholder="linkedin">
                </div>
                <!-- end add-text -->
                <div class="add-text col-xs-12">
                    <input type="submit" value="Save">
                </div>
                <!-- end add-text -->

            </div>
            <!-- end element-wrapper -->
        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

<?php include 'footer.php' ?>