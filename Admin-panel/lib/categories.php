<?php

function addCategory($name, $pic)
{
    global $link;
    $name = $link->real_escape_string($name);

    $query = "INSERT INTO `categories` (`name`, `pic`) ";
    $query = $query . " VALUES ('$name','$pic')";
    $insert = $link->query($query);
    print_r($insert);
    return $query;
}

function deleteCategory($id)
{
    global $link;
    $query = "DELETE FROM `categories` WHERE id=$id";
    $res = $link->query($query);
    return $res;
}

function editCategory($id, $name, $pic)
{
    global $link;

    $query = "UPDATE categories SET name='$name' , pic='$pic' where id='$id'";
    $res = $link->query($query);
    return $res;
}


function listCategories()
{
    global $link;
    $query = "select * from `categories` ORDER BY id DESC ";
    $rows = $link->query($query);
    //print_r($rows->fetch_array(MYSQL_ASSOC));
    if ($rows) {
        $rows = $rows->fetch_all(MYSQL_ASSOC);
        return $rows;
    }
}


function viewCategory($id)
{
    global $link;
    $query = "SELECT * from `categories` WHERE id=$id";
    $category = $link->query($query);
    $category = $category->fetch_all(MYSQL_ASSOC);
    return $category;
}


function getCategoryTypes($id)
{
    global $link;
    $query = "SELECT * FROM `types` WHERE category_id='$id' ORDER BY id desc";
    $types = $link->query($query);
    $types = $types->fetch_all(MYSQL_ASSOC);
    return $types;
}

function countCategories()
{
    global $link;
    $query = "SELECT COUNT(*) FROM `categories`";
    $categories = $link->query($query);
    $categories = $categories->fetch_assoc();
    return $categories['COUNT(*)'];
}