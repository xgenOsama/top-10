<?php include 'header.php';
if ($_GET['id']) {
    $id = $_GET['id'];
    $_SESSION['product_id'] = $id;
} else {
    $id = $_SESSION['product_id'];
}
if ($_POST) {
    addComparing($id, $_POST['price'], $_POST['quality'], $_POST['speed'], $_POST['apperance']);
}
$product = viewProduct($id)[0];
?>
    <div id="page-wrapper">

        <div class="container-fluid">

            <div class="element-wrapper col-xs-12">

                <div class="col-md-8 element-right col-xs-12">
                    <h1>عناصر المقارنة التي تم ادخالها</h1>

                    <form action="<?= $_SERVER['PHP_SELF'] ?>" METHOD="post">
                        <div>
                            <label>
                                <span>السعر</span>
                                <input type="text" placeholder="السعر" name="price" value="<?= $product['price'] ?>">
                            </label>
                        </div>

                        <div>
                            <label>
                                <span>الجودة</span>
                                <input type="text" placeholder="الجوده" name="quality"
                                       value="<?= $product['quality'] ?>">
                            </label>
                        </div>

                        <div>
                            <label>
                                <span>السرعة</span>
                                <input type="text" placeholder="السرعه" name="speed" value="<?= $product['speed'] ?>">
                            </label>
                        </div>

                        <div>
                            <label>
                                <span>الشكل</span>
                                <input type="text" placeholder="الشكل" name="apperance"
                                       value="<?= $product['apperance'] ?>">
                            </label>
                        </div>

                        <div>
                            <button type="submit" class="btn btn-primary">ادخل</button>
                        </div>
                    </form>
                </div>

                <div class="col-md-4 element-left col-xs-12">
<!--                    <h1 class="add-elem" data-toggle="tooltip" data-placement="top" title="Add new element">-->
<!--                        <i class="fa fa-plus"></i>-->
<!--                    </h1>-->

                    <div class="left-img col-xs-12">
                        <div class="left-caption col-xs-12">
<!--                            <div class="imgcontent col-xs-12">-->
<!--                                <div class="bstext">-->
<!--                                        <span>-->
<!--                                          <i class="fa fa-camera"></i><br>-->
<!--                                          Upload an image-->
<!--                                      </span>-->
<!--                                </div>-->
<!--                                <!-- end bstext -->
<!--                                <output id="list"></output>-->
<!--                                <input type="file" id="show-adj8" name="myFileSelect">-->
<!--                            </div>-->
                        </div>
                        <!-- end left-caption -->
                        <img src="<?= $product['pic'] ?>" alt="<?= $product['name'] ?>" class="target2">
                    </div>
                    <!-- end left-img -->
                    <div class="left-name col-xs-12">
                        <h1><?= $product['name'] ?></h1>

                    </div>
                    <!-- end left-name -->
                </div>
                <!-- end element-left -->

                <div class="edit-sector2 col-md-4 col-sm-6 col-xs-12">
                        <span class="add-elements" data-toggle="tooltip" data-placement="bottom"
                              title="Add new element">
                        <i class="fa fa-plus"></i>
                    </span>

                    <div class="elements col-xs-12">
                        <input type="text" placeholder="type comparison element name ..">
                    </div>
                    <input type="submit" value="Save">
                </div>
                <!-- end edit-sector2 -->
            </div>
            <!-- end element wrapper -->

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

<?php include 'footer.php' ?>