<?php
include 'includes.php';
if (!checklogin()) {
    header('Location: ./login.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Store | Home Cpanel</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- hover.css -->
    <link href="css/hover.css" rel="stylesheet" type="text/css">
    <!-- for alertify -->
    <link href="css/alertify.css" rel="stylesheet" type="text/css">
    <!-- for_fivicon -->
    <link href="#" type="text/css" rel="icon">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand hvr-buzz-out" href="index.php">Store </a>
        </div>

        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav text-center">
            <li class="dropdown open-user">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="images/avatar2.png"
                                                                                class="avt"><?= $_SESSION['username'] ?>
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu col-xs-12 text-center">

                    <li class="frst">
                        <img src="images/avatar2_big.png" class="image-admin" alt="image-admin">

                        <p><?= $_SESSION['username'] ?></p>
                    </li>

                    <li class="personal">
                        <a href="./logout.php" class="sgnout hvr-bounce-to-left">logout</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown open-user msgs hidden-lg ">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> Messages<b
                        class="caret"></b>
                </a>
                <ul class="dropdown-menu col-xs-12 text-center msg-box">
                    <li>
                        <b><img src="images/me.jpg" alt="" width="50" height="50"></b>

                        <div>
                            <h1>Amir Nageh</h1>
                            <span>odjsojdsmlfjvojefp,meovjohojfeojfe</span>
                        </div>
                    </li>
                    <li>
                        <b><img src="images/me.jpg" alt="" width="50" height="50"></b>

                        <div>
                            <h1>Amir Nageh</h1>
                            <span>odjsojdsmlfjvojefp,meovjohojfeojfe</span>
                        </div>
                    </li>
                    <li>
                        <b><img src="images/me.jpg" alt="" width="50" height="50"></b>

                        <div>
                            <h1>Amir Nageh</h1>
                            <span>odjsojdsmlfjvojefp,meovjohojfeojfe</span>
                        </div>
                    </li>
                    <li>
                        <b><img src="images/me.jpg" alt="" width="50" height="50"></b>

                        <div>
                            <h1>Amir Nageh</h1>
                            <span>odjsojdsmlfjvojefp,meovjohojfeojfe</span>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">

                <li>
                    <a href="categories.php" class="hvr-underline-from-left"><i class="fa fa-fw fa-suitcase"></i> &nbsp;Categories</a>
                </li>

                <li>
                    <a href="addUser.php" class="hvr-underline-from-left"><i class="fa fa-fw fa-envelope"></i> &nbsp;add User</a>
                </li>

                <li>
                    <a href="about.php" class="hvr-underline-from-left"><i class="fa fa-fw fa-suitcase"></i> &nbsp;About
                        Site</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
