<?php include 'header.php';
if ($_GET['id']) {
    $id = $_GET['id'];
    $_SESSION['category_id'] = $id;
} else {
    $id = $_SESSION['category_id'];
}
$types = getCategoryTypes($id);
?>

    <div class="types-head col-xs-12 text-center">
        <div class="container">
            <h1>كــل الأصناف</h1>
        </div>
        <!-- end container -->
    </div>
    <!-- end types-head -->
    <div class="types-wrapper col-xs-12">
        <div class="container">
            <?php foreach ($types as $type): ?>
                <div class="block col-md-3 col-sm-6 col-xs-12">
                    <div class="block-inner col-xs-12">
                        <div class="block-caption col-xs-12 text-center">
                            <a href="<?=$type['pic']?>" class="preview" rel="prettyPhoto">
                                <i class="fa fa-search"></i>
                            </a>
                            <a href="comparison.php" class="out-link">
                                <i class="fa fa-paper-plane"></i>
                            </a>
                        </div>
                        <!-- end block-caption -->
                        <div class="block-img">
                            <img src="<?=$type['pic']?>" alt="" class="img-responsive">
                        </div>
                        <!-- end block-img -->
                        <div class="block-data">
                            <h1><?=$type['name']?></h1>
                        </div>
                        <!-- end block-data -->
                    </div>
                    <!-- end block-inner -->
                </div>
            <?php endforeach ?>
        </div>
        <!-- end container -->
    </div>
<?php include 'footer.php' ?>