<?php include 'header.php';
if ($_GET['id']) {
    $id = $_GET['id'];
    $_SESSION['type_id'] = $id;
} else {
    $id = $_SESSION['type_id'];
}
$products = productsByPrice($id);
?>

<div class="comparison-head col-xs-12 text-center">
    <div class="container">
        <h1>مقارنة بين افضل عشرة أصناف</h1>
    </div>
    <!-- end container -->
</div>
<!-- end comparison-head -->
<div class="compare-filter col-xs-12">
    <div class="filters col-xs-12">
        <a id="price" class="active show-compare1">السعر</a>
        <a id="quality" class="show-compare2">الجوده</a>
        <a id="speed">السرعه</a>
        <a id="apperance">الشكل</a>
    </div>
</div>

<div class="comparison-wrapper col-xs-12">
    <div class="container">
        <div class="compare-slider col-xs-12" id="products_body">
            <?php foreach ($products as $product): ?>
                <div class="card">
                    <div class="card-inner text-center">
                        <div class="card-head">
                            <h1><?= $product['name'] ?></h1>
                        </div>
                        <!-- end card-head -->
                        <div class="card-img">
                            <img src="<?= $product['pic'] ?>" alt="">
                        </div>
                        <!-- end card-img -->
                        <div class="card-name">
                            <a href="#"><?= $product['name'] ?></a>
                        </div>
                        <!-- end card-name -->
                    </div>
                    <!-- end card-inner -->
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>
<!-- end compare-slider -->

<div class="compare-filter col-xs-12">
    <div class="container">
        <canvas id="myChart" width="1150" height="400"></canvas>
    </div>
</div>
<!-- end compare-filter -->
</div>
<!-- end container -->
</div>
<!-- end comparison-wrapper -->

<?php include 'footer.php'; ?>
<script type="application/javascript">
    $(document).ready(function () {
        function getData(names, values) {
            var data = {
                labels: names,
                datasets: [
                    {
                        label: "Comparison",
                        fillColor: "rgba(151,187,205,0.5)",
                        strokeColor: "rgba(151,187,205,0.8)",
                        highlightFill: "rgba(151,187,205,0.75)",
                        highlightStroke: "rgba(151,187,205,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: values
                    }
                ]
            };
            return data;
        }

        function getOptions() {
            var options = {

                ///Boolean - Whether grid lines are shown across the chart
                scaleShowGridLines: true,

                //String - Colour of the grid lines
                scaleGridLineColor: "rgba(0,0,0,.05)",

                //Number - Width of the grid lines
                scaleGridLineWidth: 1,

                //Boolean - Whether to show horizontal lines (except X axis)
                scaleShowHorizontalLines: true,

                //Boolean - Whether to show vertical lines (except Y axis)
                scaleShowVerticalLines: true,

                //Boolean - Whether the line is curved between points
                bezierCurve: true,

                //Number - Tension of the bezier curve between points
                bezierCurveTension: 0.4,

                //Boolean - Whether to show a dot for each point
                pointDot: true,

                //Number - Radius of each point dot in pixels
                pointDotRadius: 4,

                //Number - Pixel width of point dot stroke
                pointDotStrokeWidth: 1,

                //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
                pointHitDetectionRadius: 20,

                //Boolean - Whether to show a stroke for datasets
                datasetStroke: true,

                //Number - Pixel width of dataset stroke
                datasetStrokeWidth: 2,

                //Boolean - Whether to fill the dataset with a colour
                datasetFill: true,

                //String - A legend template
                legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"

            };
            return options;
        }

        function draw(data, options) {
            var ctx = document.getElementById("myChart").getContext("2d");
            var myLineChart = new Chart(ctx).Line(data, options);
        }

        function getNames(data) {
            var names = [];
            data.forEach(function (item) {
                names.push(item.name);
            });
            return names;
        }

        function getPrices(data) {
            var prices = [];
            data.forEach(function (item) {
                prices.push(item.price);
            });
            return prices;
        }

        function getQualities(data) {
            var qualities = [];
            data.forEach(function (item) {
                qualities.push(item.quality);
            });
            return qualities;
        }

        function getSpeeds(data) {
            var speeds = [];
            data.forEach(function (item) {
                speeds.push(item.speed);
            });
            return speeds;
        }

        function getApperances(data) {
            var apperances = [];
            data.forEach(function (item) {
                apperances.push(item.apperance);
            });
            return apperances;
        }

        function listProducts(name, pic) {
            var obj = $('#products_body');
            obj.get(0).innerHTML += '<div class="owl-item" style="width: 114px;"><div class="card"> <div class="card-inner text-center"> <div class="card-head"> <h1>' + name + '</h1> </div> <div class="card-img"> <img src="' + pic + '"  alt=""> </div> <div class="card-name"> <a href="#">' + name + '</a> </div> </div> </div></div>';
        }




        $('#quality').on('click', function (e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "./ajax/getByQuality.php",
                data: {'type_id': <?=$id ?>},
                success: function (data) {
                    $('#products_body').get(0).innerHTML = "";
                    data.forEach(function (item) {
                        listProducts(item.name, item.pic);
                    });
                    var names = getNames(data);
                    var values = getQualities(data);
                    var data = getData(names, values);
                    var options = getOptions();
                    draw(data, options);
                },
                dataType: 'json'
            })
        });

        $('#price').on('click', function (e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "./ajax/getByPrice.php",
                data: {'type_id': <?=$id ?>},
                success: function (data) {
                    $('#products_body').get(0).innerHTML = "";
                    data.forEach(function (item) {
                        listProducts(item.name, item.pic);
                    });
                    var names = getNames(data);
                    var values = getPrices(data);
                    var data = getData(names, values);
                    var options = getOptions();
                    draw(data, options);
                },
                dataType: 'json'
            });
        });

        $('#speed').on('click', function (e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "./ajax/getBySpeed.php",
                data: {'type_id': <?=$id ?>},
                success: function (data) {
                    $('#products_body').get(0).innerHTML = "";
                    data.forEach(function (item) {
                        listProducts(item.name, item.pic);
                    });
                    var names = getNames(data);
                    var values = getSpeeds(data);
                    var data = getData(names, values);
                    var options = getOptions();
                    draw(data, options);
                },
                dataType: 'json'
            })
        });


        $('#apperance').on('click', function (e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "./ajax/getByApperance.php",
                data: {'type_id': <?=$id ?>},
                success: function (data) {
                    $('#products_body').get(0).innerHTML = "";
                    data.forEach(function (item) {
                        listProducts(item.name, item.pic);
                    });
                    var names = getNames(data);
                    var values = getApperances(data);
                    var data = getData(names, values);
                    var options = getOptions();
                    draw(data, options);
                },
                dataType: 'json'
            })
        });

    });

</script>
