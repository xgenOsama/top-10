<?php
require '../includes.php';
if (isset($_POST['category_id'])) {
    $types = getCategoryTypes($_POST['category_id']);
    echo json_encode($types);
}