<?php
require '../includes.php';

if ($_POST['type_id']) {
    $products = productsBySpeed($_POST['type_id']);
    shuffle($products);
    echo json_encode($products);
}