<?php
require '../includes.php';

if ($_POST['type_id']) {
    $products = productsByApperance($_POST['type_id']);
    shuffle($products);
    echo json_encode($products);
}