<?php

function addType($name, $pic, $category_id)
{
    global $link;
    $name = $link->real_escape_string($name);

    $query = "INSERT INTO `types` (`name`, `pic`,`category_id`) ";
    $query = $query . " VALUES ('$name','$pic','$category_id')";
    $insert = $link->query($query);
    print_r($insert);
    return $query;
}

function deleteType($id)
{
    global $link;
    $query = "DELETE FROM `types` WHERE id=$id";
    $res = $link->query($query);
    return $res;
}

function editType($id, $name, $pic)
{
    global $link;

    $query = "UPDATE types SET name='$name' , pic='$pic' where id='$id'";
    $res = $link->query($query);
    return $res;
}


function listTypes()
{
    global $link;
    $query = "select * from `types` ORDER BY id DESC ";
    $rows = $link->query($query);
    //print_r($rows->fetch_array(MYSQL_ASSOC));
    if ($rows) {
        $rows = $rows->fetch_all(MYSQL_ASSOC);
        return $rows;
    }
}


function viewTypes($id)
{
    global $link;
    $query = "SELECT * from `types` WHERE id=$id";
    $type = $link->query($query);
    $type = $type->fetch_all(MYSQL_ASSOC);
    return $type;
}

function countTypes()
{
    global $link;
    $query = "SELECT COUNT(*) from `types`";
    $types = $link->query($query);
    $types = $types->fetch_assoc();
    return $types['COUNT(*)'];
}