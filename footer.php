
<div class="footer col-xs-12 text-center">
    <div class="container">
        <div class="col-md-6 navbar-right">
            <p> Site Name جميع الحقوق محفوظه لدي</p>
        </div>
        <div class="col-md-6 navbar-left">
            <a href="contact-us.php">تواصل معنا </a>
            <a href="about-us.php">عن الموقع</a>
            <a href="index.php">الرئيسية</a>
        </div>
    </div>
    <!-- end container -->
</div>
<!-- end footer -->

<!-- JavaScript Files -->
<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/fancySelect.js" type="text/javascript"></script>
<script src="js/owl.carousel.min.js" type="text/javascript"></script>
<script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>
<script src="js/canvasjs.min.js" type="text/javascript"></script>
<script src="charts/Chart.js" type="text/javascript"></script>
</body>

</html>
